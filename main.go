package main

import (
	"image"
	"image/color"
	"math/rand"
	"os"
	"time"

	"github.com/banthar/Go-SDL/sdl"
	"github.com/banthar/Go-SDL/ttf"
	"github.com/thefryscorer/gocontroller"
)

const (
	screenWidth  = 1366
	screenHeight = 768
	screenBPP    = 32
	screenFlags  = sdl.FULLSCREEN

	playerSpeed    = 5
	gameFPS        = 30
	PLAYERWINSCORE = 5
)

var (
	playerColors = []color.NRGBA{
		{255, 255, 255, 255},
		{128, 255, 255, 255},
		{255, 128, 255, 255},
		{255, 255, 128, 255},
	}
)

// Helper functions.

// sdlBlit draws the first argument on top of the second argument, with the first argument's
// top-left corner located at (x, y).
func sdlBlit(surf *sdl.Surface, screen *sdl.Surface, x int, y int) {
	rect := sdl.Rect{
		X: int16(x),
		Y: int16(y),
		W: uint16(surf.W),
		H: uint16(surf.H),
	}
	screen.Blit(&rect, surf, nil)
}

// makeBall returns an SDL surface containing an image of a ball with the given radius and color.
func makeBall(radius int, col color.Color) *sdl.Surface {
	img := image.NewNRGBA(image.Rect(0, 0, radius*2, radius*2))
	for x := -radius; x < radius; x++ {
		for y := -radius; y < radius; y++ {
			if x*x+y*y < radius*radius {
				img.Set(radius+x, radius+y, col)
			}
		}
	}
	return sdl.CreateSurfaceFromImage(img)
}

func colorToUInt32(c color.NRGBA, screenSurf *sdl.Surface) uint32 {
	r := c.R
	g := c.G
	b := c.B
	a := c.A

	result := sdl.MapRGBA(screenSurf.Format, r, g, b, a)

	return result
}

// Player Management.

type Player struct {
	x, y   int
	dX, dY int
	ip     string
	color  color.NRGBA
	surf   *sdl.Surface
	score  int
}

func (p *Player) processInput(in gocontroller.Input) {
	switch in.Key {
	case "Up":
		if in.Event == gocontroller.PRESS {
			p.dY = -playerSpeed
		} else if in.Event == gocontroller.RELEASE {
			p.dY = 0
		}
	case "Down":
		if in.Event == gocontroller.PRESS {
			p.dY = playerSpeed
		} else if in.Event == gocontroller.RELEASE {
			p.dY = 0
		}
	case "Left":
		if in.Event == gocontroller.PRESS {
			p.dX = -playerSpeed
		} else if in.Event == gocontroller.RELEASE {
			p.dX = 0
		}
	case "Right":
		if in.Event == gocontroller.PRESS {
			p.dX = playerSpeed
		} else if in.Event == gocontroller.RELEASE {
			p.dX = 0
		}
	case "A":
		if in.Event == gocontroller.RELEASE {
			p.color = color.NRGBA{R: uint8(rand.Intn(255)), G: uint8(rand.Intn(255)), B: uint8(rand.Intn(255)), A: 255}
			p.surf = makeBall(16, p.color)
		}
	}
}

func (p *Player) update() {
	p.x += p.dX
	p.y += p.dY
	if p.x < 0 || p.x+int(p.surf.W) > screenWidth {
		p.x -= p.dX
	}
	if p.y < 0 || p.y+int(p.surf.H) > screenHeight {
		p.y -= p.dY
	}
}

func newPlayer(ip string) Player {
	col := playerColors[rand.Intn(len(playerColors))]
	return Player{
		x:     screenWidth/2 - 8,
		y:     screenHeight/2 - 8,
		ip:    ip,
		color: col,
		surf:  makeBall(16, col),
		score: 0,
	}
}

type Food struct {
	x    int
	y    int
	surf *sdl.Surface
}

func createFood() *Food {
	food := Food{
		x:    rand.Intn(screenWidth),
		y:    rand.Intn(screenHeight),
		surf: makeBall(8, color.NRGBA{255, 255, 255, 255}),
	}

	return &food
}

func (f *Food) move() {
	f.x = rand.Intn(screenWidth)
	f.y = rand.Intn(screenHeight)
}

func (p *Player) collidesWithFood(f *Food) bool {
	var (
		aX1 int = p.x
		aX2 int = p.x + int(p.surf.W)
		aY1 int = p.y
		aY2 int = p.y + int(p.surf.H)
		bX1 int = f.x
		bX2 int = f.x + int(f.surf.W)
		bY1 int = f.y
		bY2 int = f.y + int(f.surf.H)
	)

	if aX1 < bX2 && aX2 > bX1 && aY1 < bY2 && aY2 > bY1 {
		return true
	}

	return false
}

func playerWin(player *Player, screenSurf *sdl.Surface, font *ttf.Font, inAgg *gocontroller.InputAggregator) {
	text := ttf.RenderText_Solid(font, "WINNER", sdl.Color{255, 255, 255, 255})
	for {
		screenSurf.FillRect(&screenSurf.Clip_rect, colorToUInt32(player.color, screenSurf))
		sdlBlit(text, screenSurf, int(screenSurf.W/2-text.W/2), int(screenSurf.H/2-text.H/2))
		screenSurf.Flip()
		inAgg.Collect()
		for _, in := range inAgg.Inputs {
			if in.Key == "Start" {
				runGame(screenSurf, inAgg)
			}
		}
	}
}

func runGame(screenSurf *sdl.Surface, inAgg *gocontroller.InputAggregator) {
	//Load font
	font := ttf.OpenFont("font.otf", 24)

	players := make([]Player, 0)

	f := createFood()

	frameTime := 1.0 / gameFPS * float64(time.Second)
	ticker := time.NewTicker(time.Duration(frameTime))

	for {
		inAgg.Collect()
		for _, in := range inAgg.Inputs {
			// Find the player associated with the input (if any).
			found := false
			for i := 0; i < len(players); i++ {
				if players[i].ip == in.UserIP {
					players[i].processInput(in)
					found = true
					break
				}
			}

			// If no player was found, create a new one.
			if !found && in.Key == "Start" {
				players = append(players, newPlayer(in.UserIP))
			}
		}

		// Update the player positions.
		for i := 0; i < len(players); i++ {
			players[i].update()
		}

		// Move food if player collides
		for i := 0; i < len(players); i++ {
			if players[i].collidesWithFood(f) {
				f.move()
				players[i].score++
			}
		}

		// If a player has a score of 10, they win!
		for i := 0; i < len(players); i++ {
			if players[i].score > PLAYERWINSCORE {
				playerWin(&players[i], screenSurf, font, inAgg)
			}
		}

		// Clear the screen.
		screenSurf.FillRect(&screenSurf.Clip_rect, 0)

		// Redraw players.
		for _, p := range players {
			sdlBlit(p.surf, screenSurf, p.x, p.y)
		}

		// Draw food
		sdlBlit(f.surf, screenSurf, f.x, f.y)

		// Update screen.
		screenSurf.Flip()

		//Clear inputs
		inAgg.Clear()

		//Check for SDL quit
		for ev := sdl.PollEvent(); ev != nil; ev = sdl.PollEvent() {
			if _, ok := ev.(*sdl.QuitEvent); ok {
				os.Exit(0)
			}
		}

		// Keep the framerate reasonable.
		<-ticker.C
	}
}

func main() {

	layout := gocontroller.Layout{Style: gocontroller.DefaultCSS, Buttons: []gocontroller.Button{
		{Left: 20, Top: 20, Key: "Up"},
		{Left: 20, Top: 60, Key: "Down"},
		{Left: 10, Top: 40, Key: "Left"},
		{Left: 30, Top: 40, Key: "Right"},
		{Left: 45, Top: 10, Key: "Start"},
		{Left: 75, Top: 40, Key: "A", Color: "#872828"},
	}}
	server := gocontroller.NewServer(layout, gocontroller.DefaultPort)
	server.Start()

	sdl.Init(sdl.INIT_VIDEO)
	ttf.Init()
	screenSurf := sdl.SetVideoMode(screenWidth, screenHeight, screenBPP, screenFlags)
	sdl.WM_SetCaption("Controller Demo", "none")
	defer sdl.Quit()

	inAgg := server.NewInputAggregator()

	runGame(screenSurf, &inAgg)
}
